import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class launchGoogleTest
{
    WebDriver driver = null;

    @BeforeMethod
    public void launchChromeBrowser()
    {
      //  WebDriverManager.chromedriver().setup();
        System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"/Executables/ChromeDriver/Chrome76/chromedriver.exe");
        driver = new ChromeDriver();
    }
    @Test
    public void googletest()
    {
        driver.manage().window().maximize();
        driver.get("http://www.google.com/");
        System.out.println(driver.getTitle().toString());
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }
    @AfterMethod
    public void tearDown()
    {
        driver.close();
    }

}
